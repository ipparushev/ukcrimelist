import { TestBed } from '@angular/core/testing';

import { PoliceukService } from './policeuk.service';

describe('PoliceukService', () => {
  let service: PoliceukService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PoliceukService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
