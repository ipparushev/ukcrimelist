import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

type Geocode = {
	geometry: {
		location: {
			lat: number,
			lng: number
		}
	}
}
interface IGeocodeRes {
	results: Geocode[]
}

@Injectable({
	providedIn: 'root'
})
export class GooglemapsService {
	private readonly API_KEY = 'AIzaSyCt-NO0AgMyoZfNIylvwe8S1KEs9718m8Y';
	private readonly MAPS_API = 'https://maps.googleapis.com/maps/api';

	constructor(
		private httpClient: HttpClient
	) { }

	public geocode(address: string) {
		return this.httpClient.get(
			`${this.MAPS_API}/geocode/json?address=${address}&components=country:GB&key=${this.API_KEY}`
		).pipe(map((res: IGeocodeRes) => res.results[0]));
	}
}
