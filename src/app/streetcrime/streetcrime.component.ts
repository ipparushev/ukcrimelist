/// <reference types="@types/googlemaps" />
import { Component, OnInit } from '@angular/core';
import { PoliceukService } from '../policeuk.service';
import { GooglemapsService } from '../googlemaps.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';


@Component({
	selector: 'app-streetcrime',
	templateUrl: './streetcrime.component.html',
	styleUrls: ['./streetcrime.component.scss']
})
export class StreetcrimeComponent implements OnInit {
	public fetchingData = false;
	public crimeData = [];
	public searchInput = new FormGroup({
		address: new FormControl(),
		date: new FormControl(),
	});

	constructor(
		private policeService: PoliceukService,
		private gMapsService: GooglemapsService,
		public fb: FormBuilder
	) {
		this.searchInput = fb.group({
			"address": [""],
			"date": [this.defaultMonth],
		});
	}

	ngOnInit() {
		this.addressAutocomplete();
	}

	onSearchCrimers() {
		const { address, date } = this.searchInput.value;
		this.fetchingData = true;
		this.gMapsService.geocode(address).subscribe(geocode => {
			const { location } = geocode.geometry;

			this.policeService.fetchCrimeData(location, date).subscribe(crimeData => {
				this.fetchingData = false;
				this.crimeData = [...crimeData];

			})
		})
	}

	private addressAutocomplete() {
		const input = document.getElementById('address');
		const autocomplete = new google.maps.places.Autocomplete(
			(input as HTMLInputElement),
			{
				componentRestrictions: { country: 'GB' },
			}
		);
		google.maps.event.addListener(autocomplete, 'place_changed', () => {
			const place = autocomplete.getPlace();
			this.searchInput.patchValue({
				address: place.formatted_address,
			});
		});
	}

	get defaultMonth() {
		return `${new Date().getFullYear()}-${new Date().getMonth() + 1}`;
	}
}
