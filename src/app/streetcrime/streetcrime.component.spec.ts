import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreetcrimeComponent } from './streetcrime.component';

describe('StreetcrimeComponent', () => {
  let component: StreetcrimeComponent;
  let fixture: ComponentFixture<StreetcrimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreetcrimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreetcrimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
