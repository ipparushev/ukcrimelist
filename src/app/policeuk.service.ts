import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


export type Crime = {
	category: string,
	location_type: string,
	location: {
		latitude: number,
		longitude: number,
		street: {
			id: number,
			name: string,
		},
	},
	context: string,
}


@Injectable({
	providedIn: 'root'
})
export class PoliceukService {

	constructor(
		private httpClient: HttpClient
	) { }

	public fetchCrimeData(location: { lat: number, lng: number }, date: string) {
		return this.httpClient.get(
			`https://data.police.uk/api/crimes-street/all-crime?lat=52.629729&lng=-1.131592&date=2017-01`
		).pipe(map((res: Crime[]) => res));
	}
}
